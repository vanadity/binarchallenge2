package service;

import java.util.Scanner;

public class MenuImpl implements Menu {
    Scanner input = new Scanner(System.in);
    MMM mmm = new MMM();
    String readCsv = "src/main/resources/data_sekolah.csv"; //path csv
    String writeMMM = "src/main/resources/mmm.txt"; //path mean, median, modus
    String writeModus = "src/main/resources/modussekolah.txt"; //path modus sekolah

    @Override
    public void switchMenu() {

        switch (mainMenu()) {
            case 0:
                System.out.println("Proses keluar program...");
                System.exit(0);
                break;
            case 1:
                mmm.write(writeMMM);
                System.out.println("--------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Siswa");
                System.out.println("--------------------------------");
                System.out.println(("File telah di generate di " + writeMMM));
                break;
            case 2:
                mmm.writeMod(writeModus);
                System.out.println("--------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Siswa");
                System.out.println("--------------------------------");
                System.out.println(("File telah di generate di " + writeModus));
                break;
            case 3:
                mmm.write(writeMMM);
                mmm.writeMod(writeModus);
                System.out.println("--------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Siswa");
                System.out.println("--------------------------------");
                System.out.println(("File telah di generate di " + writeMMM + " dan " + writeModus));
                break;
            default:
                System.out.println("Tolong dipilih lagi \n");
                switchMenu();
                break;
        }
    }

    @Override
    public int mainMenu() {
        System.out.println("--------------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println(("File yang akan dibaca berada di " + readCsv));
        System.out.println("--------------------------------");
        System.out.println("1. Generate txt Mean, Median, Modus \n2. Generate txt Modus Sekolah \n3. Generate kedua file \n0. Keluar");

        System.out.print("---> ");
        var pilih = input.nextInt();
        return pilih;
    }
}
